#! /usr/bin/env bash

### DEPS:
### dialog
### zstd & unzstd & tar
### flatpak
### bash

### TODO:
### check if deps exist
### implement exit code checks
### restructure code
### extract methodss

# Shell settings
IFS=$'\n'

# Directories
SELFPATH="$(dirname "$(realpath -s "$0")")"
BACKUPS="$SELFPATH/backups"
TMP="$SELFPATH/tmp"
FLATPAK_CONFIGS="$HOME/.var/app"

# Include TUI components
source "$SELFPATH/user-feedback.bash"
source "$SELFPATH/prompts.bash"
source "$SELFPATH/checkbox_input.sh"

# Save user's terminal screen
printf '\e[?1049h'

# Clear screen and move cursor to (0,0)
clearScreen(){
    printf '\e[2J\e[H'
}

# Delete empty temporary folders
deleteEmptyTmpFolders() {
    echo "NYI"
}


# Main Menu
Menu="$(dialog --title "Flatup" --menu "Choose an action" 0 0 0 \
"Backup" "Backup Flatpak Apps" \
"Restore" "Restore Flatpak Apps" 3>&1 1>&2 2>&3)"

# Backup
backupFunction() {
    backupSelectionArgs=( --separate-output --checklist "Select Flatpak Apps to Backup:" 0 0 0)
    flatpakAppNameList=($(flatpak list --app --columns=name))
    flatpakAppCodeList=($(flatpak list --app --columns=application))
    
    for i in ${!flatpakAppNameList[@]}; do
        backupSelectionArgs+=("${flatpakAppCodeList[$i]}" "${flatpakAppNameList[$i]}" OFF) # change to ON for release
    done
    
    BackupSelection=("$(dialog "${backupSelectionArgs[@]}" 3>&1 1>&2 2>&3)")

    currentDateTime="$(date +"%Y-%m-%d %H.%M.%S")"
    temporaryBackupFolder="$TMP/backup/$currentDateTime"
    [ -d "$temporaryBackupFolder" ] || mkdir -p "$temporaryBackupFolder"

    backupTarget="$BACKUPS/$currentDateTime"

    [ -d "$BACKUPS" ] || mkdir -p "$BACKUPS"


    printf "%s\n" "${BackupSelection[@]}" > "$temporaryBackupFolder/flatpak-apps.txt"

    for folder in ${BackupSelection[@]}; do
        cp -r "$FLATPAK_CONFIGS/$folder" "$temporaryBackupFolder"
    done

    pushd "$temporaryBackupFolder" > /dev/null
    tar -I 'zstd -8 -v' -cf "$backupTarget.tar.zst" * 2>&1 | dialog --programbox "Compressing Backup" 20 100 3>&1 1>&2 2>&3
  
    if dialog --yes-button "Delete" --no-button "Keep" --defaultno --title "Delete temporary backup folder?" --yesno "Would you like to delete the temporary, uncompressed backup folder?\nA compressed file has already been created." 7 60;
    then
        rm -rf "$temporaryBackupFolder"
    fi
    popd > /dev/null

    if ! [ "$(ls -A "$TMP/backup")" ]; then
        rmdir "$TMP/backup"
        if ! [ "$(ls -A "$TMP")" ]; then
            rmdir "$TMP"
        fi
	fi

}

# Restore
restoreFunction() {
    restoreFile="$(dialog --title "File Selection" --fselect $HOME/ 20 100 3>&1 1>&2 2>&3)"

    until $(dialog --title "File Selection" --yesno "Is this the correct file?\n$restoreFile" 7 60 3>&1 1>&2 2>&3)
    do
        restoreFile="$(dialog --title "File Selection" --fselect $HOME/ 20 100 3>&1 1>&2 2>&3)"
    done

    temporaryRestoreFolder="$TMP/restore/$(basename $restoreFile .tar.zst)"
    [ -d "$temporaryRestoreFolder" ] || mkdir -p "$temporaryRestoreFolder"
    tar -C "$temporaryRestoreFolder" --use-compress-program=unzstd -xvf "$restoreFile" || dialog --title "Error" --msgbox "File could not be extracted." 0 0;
    
    cat "$temporaryRestoreFolder/flatpak-apps.txt" | xargs -n2 flatpak install -y --noninteractive --app 2>&1 | dialog --programbox "Installing Applications" 20 100 3>&1 1>&2 2>&3
    
    [ -d "$FLATPAK_CONFIGS" ] || mkdir -p "$FLATPAK_CONFIGS"
    
    for folder in "$temporaryRestoreFolder/*/" ; do
        cp -rfv $folder $FLATPAK_CONFIGS/ 2>&1 | dialog --programbox "Restoring Application Data" 20 100 3>&1 1>&2 2>&3
    done
}

# Main Menu Switch
case $Menu in
    Backup)
        backupFunction
        ;;
    Restore)
        restoreFunction
        ;;
    *)
        echo "Main Menu Error"
        ;;
esac

# Restore user's terminal screen
clearScreen
printf '\e[?1049l'
